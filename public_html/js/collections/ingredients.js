/*global Backbone */
var app = app || {};

(function () {
    'use strict';

    var Ingredients = Backbone.Collection.extend({
        model: app.Ingredient

    });

    app.ingredients = new Ingredients();
})();


